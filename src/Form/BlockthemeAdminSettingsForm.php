<?php

namespace Drupal\blocktheme\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Theme\Registry;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Create form for admin page.
 */
class BlockthemeAdminSettingsForm extends ConfigFormBase {

  /**
   * Returns the theme.registry service.
   *
   * @var \Drupal\Core\Theme\Registry
   */
  protected $themeRegistry;

  /**
   * Constructs a BlockthemeAdminSettingsForm form.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Theme\Registry $theme_registry
   *   Defines the theme registry service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, Registry $theme_registry) {
    parent::__construct($config_factory);

    $this->themeRegistry = $theme_registry;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('theme.registry')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'blocktheme.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'blocktheme_admin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('blocktheme.settings');

    $form['help'] = [
      '#markup' => t('BlockTheme allows an admin to define twig files for standard block templates and provides a select box on the block configure form so the user can select a twig file to use as opposed to having to override the templates by block ID.'),
    ];

    $form['templates'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Custom Block Templates'),
      '#description' => $this->t('Enter pairs of template suggestions, where machine name corresponds to a twig file called <em>block--blocktheme--{MACHINE_NAME}.html.twig</em>.'),
      '#tree' => TRUE,
      '#prefix' => '<div id="templates-fieldset-wrapper">',
      '#suffix' => '</div>',
    ];

    $count = $form_state->get('templates_count');
    if (empty($count)) {
      $config_count = !empty($config->get('templates')) ? count($config->get('templates')) : 1;

      $form_state->set('templates_count', $config_count);
      $count = $config_count;
    }

    for ($i = 0; $i < $count; $i++) {
      $form['templates'][$i]['name'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Template machine name #@number', ['@number' => $i + 1]),
        '#default_value' => !empty($config->get('templates')[$i]['name']) ? $config->get('templates')[$i]['name'] : '',
      ];
      $form['templates'][$i]['label'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Template label #@number', ['@number' => $i + 1]),
        '#default_value' => !empty($config->get('templates')[$i]['label']) ? $config->get('templates')[$i]['label'] : '',
      ];
    }

    $form['templates']['actions'] = [
      '#type' => 'actions',
    ];

    $form['templates']['actions']['template'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add new suggestion'),
      '#submit' => ['::addMore'],
      '#ajax' => [
        'callback' => '::addMoreCallback',
        'wrapper' => 'templates-fieldset-wrapper',
      ],
    ];

    $form['show'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show Custom Block Theme'),
      '#description' => $this->t('Show the custom block theme used for a block in the %block_admin_page.', [
        '%block_admin_page' => Link::fromTextAndUrl('block admin page', Url::fromRoute('block.admin_display'))->toString(),
      ]),
      '#default_value' => $config->get('show'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Ajax handler for the 'Add more' button.
   */
  public function addMoreCallback(array &$form, FormStateInterface $form_state) {
    return $form['templates'];
  }

  /**
   * Submit handler for the 'Add more' button.
   */
  public function addMore(array &$form, FormStateInterface $form_state) {
    $form_state->set('templates_count', $form_state->get('templates_count') + 1);
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $templates = [];
    foreach ($form_state->getValue('templates') as $key => $data) {
      if (is_numeric($key) && !empty($data['name']) && !empty($data['label'])) {
        $templates[] = $data;
      }
    }

    $this->config('blocktheme.settings')
      ->set('templates', $templates)
      ->set('show', (bool) $form_state->getValue('show'))
      ->save();

    parent::submitForm($form, $form_state);
    $this->themeRegistry->reset();
  }

}
