# Block Theme

BlockTheme allows an admin to define tpl files for standard block templates
and provides a select box on the block configure form so the user can select
a tpl file to use as opposed to having to override the templates by block ID.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/blocktheme).

To submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/blocktheme).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Enable the module

2. Go to /admin/structure/block/blocktheme and add entries like:
custom_template|My Custom Template
my_super_template|My SuperTemplate
Where the first name is the machine-readable name of your template which may contain only
alphanumerical characters, -, or _ . The second name is the user-friendly name that appears
in the select inbox on the block edit form.

3. Choose from either step 4. or 5.

4. Create twig files in your theme directory like: (Note: filenames must be preceded by block--blocktheme--)
 block--blocktheme--custom-template.html.twig
 block--blocktheme--my-super-template.html.twig

5. Alternatively use the extra provided variable $blocktheme to customize your
 block.html.twig or block-*.html.twig files. The $blocktheme will typically be
 used as a css class name in you template and contains the machine-readable name of
 your template.

6. In your custom block templates, you can also use values from the
 $blocktheme_vars variable. This array contains custom variables defined in the
 block edit form, right below the block theme selection.


## Maintainers

- Andrei Mateescu - [amateescu](https://www.drupal.org/u/amateescu)
- Greg Harvey - [greg.harvey](https://www.drupal.org/u/gregharvey)
- Jacob Singh - [JacobSingh](https://www.drupal.org/u/jacobsingh)
- Alexander Shudra - [str8](https://www.drupal.org/u/str8)
- Viktor Holovachek - [AstonVictor](https://www.drupal.org/u/astonvictor)
